"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const mongo = require('./MongoServer');
let app = express();
let recipeList = [];
let fridge = [];
let selectedRecipe;
let url = 'mongodb://localhost:27017';
let dbName = 'recipeSystem';
let mongoConnector = new mongo.RecipeConnector(url,dbName);
var corsOptions = {
    origin: '*',
    optionsSuccessStatus: 200 
};
app.use(cors(corsOptions));
app.use(bodyParser.json()); 

app.param('name', function(req,res,next,value){
    req.data = req.data || {};
    req.data.name = value;
    next();
});

app.param('recipeName', function(req,res,next,value){
    req.data = req.data || {};
    req.data.recipeName = value;
    next();
});
app.post("/:name/addInstruction",function(req,res){
    console.log("in the function",req.body);
    mongoConnector.addInstrution(req.data.name,req.body.in)
    .then((result => {
        res.send({instruction : result})
    }))
});

app.post("/addRecipe", function (req, res) {
    mongoConnector.addRecipe(req.body)
    .then((recipeList) => {
        console.log("recipe added");
        res.send({ recipes : recipeList });
    })
    .catch((reason) => {
        console.log("Error: cannot add recipe");
        res.status(500);
        res.render('error',{error : reason});
    }) 
});

app.get("/getRecipeList",function(req,res){
    console.log("getting recipeList");
    mongoConnector.getRecipeList()
    .then((result) => {
        //console.log(result);
        res.send({recipes : result});
    })
    .catch((reason) => {
        res.status(500);
        res.render('error',{error : reason});
    })  
});

app.get("/getFridgeList",function(req,res){
    console.log("getting fridgeList");
    mongoConnector.getFridgeIngredients()
    .then((result) => {
        console.log("line 71 in server.js",result);
        res.send({ingredients : result});
    })
    .catch((reason) => {
        res.status(500);
        res.render('error',{error : reason});
    })  
});

app.delete("/deleteRecipe/:name",function(req,res){
    console.log("sending request",req.data.name);
    mongoConnector.deleteRecipe(req.data.name)
    .then((result) => {
        console.log("recipe delete from mongo",result.length);
        res.send({recipes : result});
    })
    .catch((reason) => {
        res.status(500);
        res.render('error',{error : reason});
    })
    console.log("deleted");
});

app.post("/recipe/:name/addIngredient",function(req,res){
    console.log("adding Ingredient",req.body,"and ",req.data.name);
    mongoConnector.addIngredient(req.data.name,req.body)
    .then((result) => {
        console.log("ingredient added in serve.js");
        res.send({ingredients : result});        
    })
    .catch((reason) => {
        console.log("Error: adding ingredeint to mongo server failed");
        res.status(500);
        res.render('error',{error : reason});
    })
})

app.delete("/deleteIngredient/:recipeName/:name",function(req,res){
    console.log("delete request",req.data.name);

    mongoConnector.deleteRecipeIngredient(req.data.recipeName,req.data.name)
    .then((result) => {
        console.log(result);
        res.send({ingredients : result});        
    })
    .catch((reason) => {
        console.log("deleting element failed in mongo",reason);
        res.status(500);
        res.render('error',{error : reason});
    })

});

app.post("/fridge/addIngredient",function(req,res){
    console.log("adding Ingredient 121 to fridge",req.body);
    mongoConnector.addFridgeIngredient(req.body)
    .then((result) => {
        console.log("ingredient added TO fridge in serve.js");
        res.send({ingredients : result});        
    })
    .catch((reason) => {
        console.log("Error: adding ingredeint to mongo server failed");
        res.status(500);
        res.render('error',{error : reason});
    })
});

app.delete("/fridge/deleteIngredient/:name",function(req,res){
    console.log("delete request for fridge",req.data.name);
    mongoConnector.deleteFridgeIngredient(req.data.name)
    .then((result) => {
        console.log("ingredient added TO fridge in serve.js");
        res.send({ingredients : result});        
    })
    .catch((reason) => {
        console.log("Error: adding ingredeint to mongo server failed");
        res.status(500);
        res.render('error',{error : reason});
    })
});

app.listen(8000, function () {
    console.log("server started");
});
console.log("Setup script finised. Notice console.log runs before the above one.");
