

let request = require("request");

describe("sending recip name", () => {
    it("Sending Recipe", (done) => {
        let recipe ={  
            r : {name:"new", estimatedTime :15,
                instructions: ["bye"] }
            }; 
        let options = {  
            uri: "http://localhost:8000/addRecipe",
            method: "POST",
            json: recipe
        }
        request.post(options, (error, response, body) => {
            expect(body.recipes).toContain(recipe); 
            console.log("recipe sent",body.recipes);
            done(); 
        }
        )
    });

    it("selecting test", (done) => { 
        request.get("http://localhost:8000/selectTheRecipe/new", () => {
            console.log("selecting the data");                         
        });
        done();
    })
});

describe("Testing adding Instruction", () => {
    it("new Instruction test", (done) => { 
    let newInstruction = {
      in: "hello"
    } 
    let options = {  
        uri: "http://localhost:8000/addInstruction",
        method: "POST",
        json: newInstruction
    }
    request.post(options, (error, response, body) => {
        console.log(body);
        expect(body.instructions).toContain(newInstruction.in); 
        done(); 
    })
    })
});
/*
describe("Testing deleting recipe", () => {
    it("Sending Recipe", (done) => {
        let recipe ={  
            r : {name:"new", estimatedTime :15}
            }; 
        let options = {  
            uri: "http://localhost:8000/addRecipe",
            method: "POST",
            json: recipe
        }

        request.post(options, (error, response, body) => {
            console.log(body);
            expect(body.recipes).toContain(recipe); 
            done(); 
        }
        )
    });

    it("deleting Recipe", (done) => {
        let recipe ={  
            r : {name:"new", estimatedTime :15}
            };
        request.delete("http://localhost:8000/deleteRecipe/new",(error, response, body) => {
            console.log(body);
            expect(body.recipes).not.toContain(recipe); 
            done(); 
        }
        )
    });
});

describe("Testing sending an Recipe to the server", () => {

    it("Sending Recipe", (done) => {
        let recipe ={  
            r : {name:"new", estimatedTime :15}
            }; 
        let options = {  
            uri: "http://localhost:8000/addRecipe",
            method: "POST",
            json: recipe
        }
        request.post(options, (error, response, body) => {
            console.log(body);
            expect(body.recipes).toContain(recipe); 
            done(); 
        }
        )
    });
})

describe("Testing adding ingredient in recipe to the server", () => {

    it("Sending ingredient", (done) => {
        let recipe ={  
            r : {name:"new", estimatedTime :15}
            }; 
        let options = {  
            uri: "http://localhost:8000/addRecipe",
            method: "POST",
            json: recipe
        }
        request.post(options, (error, response, body) => {
            console.log(body);
            expect(body.recipes).toContain(recipe); 
            done(); 
        }
        )
    });
})*/