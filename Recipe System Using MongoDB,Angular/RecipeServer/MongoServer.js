"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

class RecipeConnector{
  constructor(url,dbName){
    this.url = url;
    this.dbName = dbName;
    console.log(dbName);
    this.dbCollections = [];  
    this.connectToDB();
 }

  connectToDB() {
    this.db = this.db || new Promise((resolve, reject) => {
      MongoClient.connect(this.url, (err, client) => {
          if (!err) {
            console.log("db was connected successfully");
            // console.log(client.db(this.dbName)); //creates db if it does not exist
            resolve(client.db(this.dbName));
          }
          else {
              reject(err);
              console.log("Error connecting");
          }
      });
    });
    return this.db;
  }

  addRecipe(recipe){
    return  this.getRecipeCollection()
    .then((recipeCollection) => {
      return new Promise((resolve,reject) => {
        recipeCollection.insertOne(recipe,null,(error,result) => {
          if(error){
            reject(error);
            console.log(error.message);
          }
          else{
            resolve(recipeCollection);
          }
        });
      });
    })
    .then((r) => {
      return this.getRecipeList();
    })
  }

  deleteRecipe(name){
    return this.getRecipeCollection()
    .then((recipeCollection) => {
      return new Promise((resolve,reject) => {
        console.log("removing from mongo");
        recipeCollection.remove({"name" : name},(error,result) => {
          if(error){
            console.log("Error: failed to delete the recipe",recipe);
            reject(error);
          }
          else{
            console.log("recipe deleted");
            resolve(this.getRecipeList());
          }
        });
      });
    });
  }

  getRecipeList(){
    return this.getRecipeCollection()
    .then((recipeCollection) => {
      return new Promise((resolve,reject) => {
        recipeCollection.find({}).toArray((error,result) => {
          if(error){
            reject(error);
            console.log("Error: can't access data",error);
          }
          else{
            resolve(result);
          }
        });
      });
    });
  }

  addIngredient(name,ingredient){
    return this.getRecipeCollection()
    .then((recipeCollection) => {
      return new Promise((resolve,reject) => {
        recipeCollection.findOne({"ingredients.name" : ingredient.name},(error,result) => {
          console.log("line 93",result);
          if(result == null){
            console.log("line 94");
            recipeCollection.update({"name" : name},{$push:{"ingredients" : ingredient }},(error,result) => {
              if(result == null){
                console.log("can't insert " + ingredient + " in recipe " + name);
                reject(error);
              }
              else{
                resolve(result);
              }
            });
          }
          else{
            console.log("updating ing with",ingredient);
            resolve(this.changeQuantity(recipeCollection,name,ingredient));    
          }
        });
      })
      .then((result) => {
        return new Promise((resolve,reject) => {
          console.log("recipe ingredients line 115");
          this.getRecipe(name).then((result) => {
            resolve(result.ingredients);
          })
        })
      });
    });
  }

  addFridgeIngredient(ingredient){
    return this.getFridgeCollection()
    .then((recipeCollection) => {
      return new Promise((resolve,reject) => {
        recipeCollection.findOne({"name" : ingredient.name},(error,result) => {
          if(result == null){
            console.log("line 125");
            recipeCollection.insertOne(ingredient,null,(error,result) => {
              if(error != null){
                console.log("can't insert " + ingredient + " in recipe " + name);
                reject(error);
              }
              else{
                this.getFridgeIngredients().then((result) => {
                  resolve(result);
                })
              }
            });
          }
          else{
            console.log("updating ing with 139",ingredient);
            this.changeFridgeQuantity(recipeCollection,ingredient)
            .then((result)=>{
              this.getFridgeIngredients().then((res) => {
                resolve(res);
              })
            })    
          }
            });
        });
    });
  }


  changeQuantity(collection,name,ingredient){
    return new Promise((resolve,reject) => {
      collection.update({"name" : name ,"ingredients.name" : ingredient.name},{$set:{"ingredients.$.quantity" : ingredient.quantity}},(error,result) => {
        if(error){
          console.log("line 146",error);
          reject(error);
        }
        else{
          console.log("line 150");
          resolve(result);
        }
      });
    });
  }

  changeFridgeQuantity(collection,ingredient){
    return new Promise((resolve,reject) => {

      collection.update({"name" : ingredient.name},{$set:{"quantity" : ingredient.quantity}},(error,result) => {
        if(error){
          console.log("line 161",error);
          reject(error);
        }
        else{
          resolve(result);
        }
      });
    });
  }
  
  deleteRecipeIngredient(name,ingredient){
    return this.getRecipeCollection()
    .then((collection) => {
      return new Promise((resolve,reject) => {
          collection.update({"name":name},{$pull : {"ingredients" : {"name" : ingredient}}},(error,result) => {
            if(error != null){
              console.log("unable to delete ingredient",error);
              reject(error);
            }
            else{
              console.log("ingredient deleted");
              this.getRecipe(name).then((result) => {
                resolve(result.ingredients);
              })
            }
          });
      })
    });
  }

  getRecipe(name){
    return this.getRecipeCollection()
    .then((collection) => {
      return new Promise((resolve,reject) => {
        collection.findOne({"name" : name},(error,array) => {
          if(array == null){
            reject(error);
          }
          else{
            resolve(array);
          }
        })
      })
    })
    .catch((reason)=>{
      console.log("catched");
    })
  }

  deleteFridgeIngredient(name){
    return this.getFridgeCollection()
    .then((collection) => {
      return new Promise((resolve,reject) => {
          collection.remove({"name" : name},(error,result) => {
            if(result == null){
              console.log("unable to delete ingredient",error);
              reject(error);
            }
            else{
              console.log("ingredient deleted");
              resolve(this.getFridgeIngredients());
            }
          });
      });
    });
  }

  getFridgeIngredients(){
    return this.getFridgeCollection()
    .then((collection) => {
      return new Promise((resolve,reject) => {
        collection.find({}).toArray((error,array) => {
          if(array == null){
            reject(error);
          }
          else{
            resolve(array);
          }
        })
      })
    })
    .catch((reason)=>{
      console.log("catched");
    })
  }

  getFridgeCollection(){
    return this.getCollection("fridgeCollection");
  }

  getRecipeCollection(){
    return this.getCollection("recipeCollection");
  }

  addInstrution(name,ins){
    console.log(ins);
    return this.getRecipeCollection()
    .then((collection) => {
      return new Promise((resolve,reject) => {
        collection.update({"name" : name},{$push:{"instructions" : ins}},(error,result) => {
          if(error){
            console.log("unable to add instruction in mongo");
            reject(error);
          }
          else{
            console.log("instrucution added to mongo");
            this.getRecipe(name).then((result)=>{
              resolve(result);
            })
          }
        })
      })
    })
  }

  getCollection(collectionName){
    this.dbCollections[collectionName] = this.dbCollections[collectionName] || new Promise((resolve,reject) => {
      this.connectToDB()
      .then((db) => {
        let collection = db.collection(collectionName);
        resolve(collection);
      })
      .catch((reason) => {
        console.log("Error: cannot get collection",reason);
        reject(reason);
      });
    });
    return this.dbCollections[collectionName];    
  }

}
exports.RecipeConnector = RecipeConnector;
  