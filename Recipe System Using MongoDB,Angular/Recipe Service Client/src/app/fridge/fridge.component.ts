import { Component, OnInit, Input, Output } from '@angular/core';
import { Fridge } from '../fridge.class';
import { Ingredient } from '../ingredient.class';
import { EventEmitter } from 'events';
import { Recipe } from '../recipe.class';
import { RecipeService } from '../recipe.service';

@Component({
  selector: 'app-fridge',
  templateUrl: './fridge.component.html',
  styleUrls: ['./fridge.component.css']
})

export class FridgeComponent implements OnInit {

	public newIngredient : Ingredient  = new Ingredient("",0);
	public changedIngredient : Ingredient = new Ingredient("",0);
	public selectedIngredient : Ingredient = null;

	constructor(public service : RecipeService){
		this.service = service;	
	}

	selectIngredient(i : Ingredient){
    this.selectedIngredient = i;
  }
	
	addIngredient(){
    this.validateQuantity(this.newIngredient);
		this.service.generateShoppigList();  
  }

  addQuantity(){
    this.selectedIngredient.add(this.changedIngredient);
		this.validateQuantity(this.selectedIngredient);
		this.service.generateShoppigList();  		
  }

  removeQuantity(){
    this.selectedIngredient.substract(this.changedIngredient);
		this.validateQuantity(this.selectedIngredient);
		this.service.generateShoppigList();  		
  }

  validateQuantity(selectedIngredient : Ingredient){
    if(selectedIngredient.quantity <= 0)
    {
      this.service.f1.ingredients.splice(this.service.f1.ingredients.indexOf(selectedIngredient),1);
      this.service.deleteFridgeIngredient(selectedIngredient);      
      this.selectedIngredient = null;
    }
    else{
      this.service.addFridgeIngredient(selectedIngredient);
    }
  }
  ngOnInit() {
  }

}
