export class Ingredient{

	constructor(public name : string, public quantity : number){

	}

	add(i2 : Ingredient){
		this.quantity = this.quantity+i2.quantity;
	}
	
	substract(i2: Ingredient){
		if(i2.quantity >= this.quantity)
			this.quantity = 0;
		else	
			this.quantity = this.quantity-i2.quantity;
	}
}	