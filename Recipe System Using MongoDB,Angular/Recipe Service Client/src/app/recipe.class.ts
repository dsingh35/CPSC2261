import {Ingredient} from "./ingredient.class"; 

export class Recipe{
	
	public ingredients : Ingredient[];
	public instructions : string[];
	public estimatedTime : number;
	public name : string;

	constructor(name:string = "", time : number = 0) {
		this.ingredients = new Array();
		this.instructions = new Array();
		this.name = name;
		this.estimatedTime = time;
	}


	addItem(i : Ingredient){
		for (let item of this.ingredients) {
			if(item.name == i.name){
				item.add(i);
				return;
			}	
		}
		this.ingredients.push(new Ingredient(i.name,i.quantity));
	}

	addInstruction(inst : string){
		this.instructions.push(inst);
	}
}