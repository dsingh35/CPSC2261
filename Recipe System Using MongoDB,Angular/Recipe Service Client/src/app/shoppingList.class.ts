import { Ingredient } from "./ingredient.class";

export class ShoppingList {
    
    constructor(public ingredients: Ingredient[]){}

}