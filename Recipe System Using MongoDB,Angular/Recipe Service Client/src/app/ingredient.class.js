"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Ingredient {
    constructor(name, quantity) {
        this.name = name;
        this.quantity = quantity;
    }
    add(i2) {
        this.quantity = this.quantity + i2.quantity;
    }
    substract(i2) {
        if (i2.quantity >= this.quantity)
            this.quantity = 0;
        else
            this.quantity = this.quantity - i2.quantity;
    }
}
exports.Ingredient = Ingredient;
//# sourceMappingURL=ingredient.class.js.map