import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ShoppingList } from '../shoppingList.class';
import { Ingredient } from '../ingredient.class';
import { RecipeService } from '../recipe.service';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit {
  public service;

  constructor(service : RecipeService) {
    this.service = service;
  }
  
  ngOnInit() {
  }

}
