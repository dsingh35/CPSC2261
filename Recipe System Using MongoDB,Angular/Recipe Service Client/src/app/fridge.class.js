"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ingredient_class_1 = require("./ingredient.class");
class Fridge {
    constructor() {
        this.shopingList = new Array();
        this.ingredients = new Array();
    }
    add(item) {
        for (let i in this.ingredients) {
            if (this.ingredients[i].name == item.name) {
                this.ingredients[i].add(item);
                return;
            }
        }
        this.ingredients.push(new ingredient_class_1.Ingredient(item.name, item.quantity));
    }
    remove(item, quantity) {
        for (let i in this.ingredients) {
            if (item.name == this.ingredients[i].name) {
                this.ingredients[i].quantity -= quantity;
                if (this.ingredients[i].quantity <= 0) {
                    this.ingredients.splice(parseInt(i), 1);
                }
                return;
            }
        }
        console.log(item.name + " is not in the fridge");
    }
    checkRecipe(recipe) {
        this.shopingList = new Array();
        let itemsAvailable = new Array();
        for (let i of recipe.ingredients) {
            for (let j in this.ingredients) {
                if (i.name == this.ingredients[j].name) {
                    if (i.quantity > this.ingredients[j].quantity) {
                        this.shopingList.push(new ingredient_class_1.Ingredient(i.name, i.quantity - this.ingredients[j].quantity));
                    }
                    itemsAvailable.push(this.ingredients[j]);
                    break;
                }
                else if (parseInt(j) == this.ingredients.length - 1)
                    this.shopingList.push(i);
            }
        }
        return [itemsAvailable, this.shopingList];
    }
    getIngredient(item) {
        for (let i of this.ingredients) {
            if (i.name == item.name)
                return i;
        }
        return 0;
    }
}
exports.Fridge = Fridge;
//# sourceMappingURL=fridge.class.js.map