import {Ingredient} from "./ingredient.class"; 
import {Recipe} from "./recipe.class"; 

export class Fridge{
	
	public ingredients : Ingredient[];
	public shopingList : Ingredient[] = new Array();	
	constructor(){
		this.ingredients = new Array<Ingredient>();
	}

	add(item : Ingredient){

		for (let i in this.ingredients) {
			if(this.ingredients[i].name == item.name){
				this.ingredients[i].add(item);
				return;
			}
		}
		this.ingredients.push(new Ingredient(item.name,item.quantity));
	}

	remove(item : Ingredient, quantity : number){
		for (let i in this.ingredients) {
			if (item.name == this.ingredients[i].name) {
				this.ingredients[i].quantity -= quantity; 

				if (this.ingredients[i].quantity <= 0) {
					this.ingredients.splice(parseInt(i),1);
				}
				return;
			}	
		}
		console.log(item.name + " is not in the fridge");
	}

	checkRecipe(recipe : Recipe){
		this.shopingList = new Array();
		let itemsAvailable : Ingredient[] = new Array();

		for (let i of recipe.ingredients) {
			for (let j in this.ingredients) {
				if(i.name == this.ingredients[j].name){
					if(i.quantity > this.ingredients[j].quantity){
						this.shopingList.push(new Ingredient(i.name,i.quantity-this.ingredients[j].quantity));
					}
					itemsAvailable.push(this.ingredients[j]);
					break;
				}
				else if(parseInt(j) == this.ingredients.length-1)
				this.shopingList.push(i);
			}
		}
		return [itemsAvailable,this.shopingList];
	}

	getIngredient(item : Ingredient){

		for(let i of this.ingredients)
		{
			if(i.name == item.name)
				return i;
		}	
	return 0;	
	}
}