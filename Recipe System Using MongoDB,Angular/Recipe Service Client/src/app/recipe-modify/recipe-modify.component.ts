import { Component, OnInit, Output } from '@angular/core';
import { Recipe } from '../recipe.class';
import { Ingredient } from '../ingredient.class';
import { EventEmitter } from 'events';
import { RecipeService } from '../recipe.service';

@Component({
  selector: 'app-recipe-modify',
  templateUrl: './recipe-modify.component.html',
  styleUrls: ['./recipe-modify.component.css']
})

export class RecipeModifyComponent implements OnInit {

  public service : RecipeService;

  constructor(service : RecipeService) { 
    this.service = service;
    this.service.generateShoppigList();  
  }

  newInstruction : string = "";

  newIngredient : Ingredient = new Ingredient("",0);
  changedIngredient : Ingredient = new Ingredient("",0);
  selectedIngredient : Ingredient = null;

  newRecipe : Recipe = new Recipe("new");
  selectedRecipe : Recipe = null;

  selectRecipe(r : Recipe){
    if(this.selectedRecipe != null)
       this.selectedIngredient = null;

    this.selectedRecipe = r;
    this.service.selectedRecipe = this.selectedRecipe;
    this.service.generateShoppigList();    
  }

  recipeList : Array<Recipe> = null;

  addRecipe(){
    this.service.recipeList.push(new Recipe(this.newRecipe.name,this.newRecipe.estimatedTime));
    this.service.init();
    this.selectRecipe(this.service.recipeList[this.service.recipeList.length-1]);
    this.service.addRecipe(this.service.recipeList[this.service.recipeList.length-1]);
  }
  
  deleteRecipe(){
    this.service.recipeList.splice(this.service.recipeList.indexOf(this.selectedRecipe),1);
    this.service.deleteRecipe(this.selectedRecipe);      
    this.selectedRecipe = null;
    this.service.selectedRecipe = this.selectedRecipe;  
  }

  selectIngrediet(i : Ingredient){
    this.selectedIngredient = i;
  }

  addIngredient(){
    this.validateQuantity(this.newIngredient);
    this.service.generateShoppigList();    
  }

  addInstruction(){
    if(this.newInstruction.length > 0){
      this.service.addInstruction(this.newInstruction);
    }  
  }

  addQuantity(){
    this.selectedIngredient.add(this.changedIngredient);
    this.validateQuantity(this.selectedIngredient);
    this.service.generateShoppigList();
  }

  removeQuantity(){
    this.selectedIngredient.substract(this.changedIngredient);
    this.validateQuantity(this.selectedIngredient);
    this.service.generateShoppigList();    
  }

  validateQuantity(selectedIngredient : Ingredient){
    if(selectedIngredient.quantity <= 0)
    {
      this.service.deleteIngredient(this.selectedIngredient);
      this.selectedRecipe.ingredients.splice(this.selectedRecipe.ingredients.indexOf(selectedIngredient),1);
      this.selectedIngredient = null;
    }
    else{
      this.service.addIngredient(selectedIngredient);  
    }
  }

  ngOnInit() {
  }
}