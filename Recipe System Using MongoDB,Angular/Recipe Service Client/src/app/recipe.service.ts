import { Injectable } from '@angular/core';
import { Recipe } from './recipe.class';
import { Ingredient } from './ingredient.class';
import { Fridge } from './fridge.class';
import { HttpRequest, HttpClient, HttpResponse, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RecipeService {
  
	public f1 : Fridge = new Fridge();

  public recipeList = [new Recipe("f",30)];
  public selectedRecipe = new Recipe("",0);

  public ingredientsToShop : Array<Ingredient> = new Array<Ingredient>();

  constructor(public httpClient:HttpClient) {
    this.init();
  }

  addRecipe( recipe : Recipe){
    this.httpClient.post("http://localhost:8000/addRecipe", recipe)
    .toPromise()
    .catch((reason) => {
      console.log("Error: sending recipe failed ",reason);
    })
    .then((response : any) => {
      console.log("fulfilled, recipe is added",response);
        let newRecipeList = response.recipes.map((value) => {
        return this.cloneRecipe(value);
    });
      this.recipeList = newRecipeList;

    })
  }

  getRecipeListFromServer(){
    this.httpClient.request(new HttpRequest("GET","http://localhost:8000/getRecipeList"))
    .toPromise()
    .then((result : any) => {
      console.log("fulfilled, recipe is added",result.body);
      let newRecipeList = result.body.recipes.map((value) => {
        return this.cloneRecipe(value);
      })
      this.recipeList = newRecipeList;
    })
    .catch((reason) => {

      console.log("failed getting recipeList",reason);
    })
  }

  getFridgeListFromServer(){
    this.httpClient.request(new HttpRequest("GET","http://localhost:8000/getFridgeList"))
    .toPromise()
    .then((result : any) => {
      let list = [];
      console.log(result)
        result.body.ingredients.map((value) => {
        list.push(new Ingredient(value.name,value.quantity));
      })
      this.f1.ingredients = list;
    })
    .catch((reason) => {
      console.log("failed getting fridgeList",reason);
    })
  }

  addFridgeIngredient( ing : Ingredient){

    this.httpClient.post("http://localhost:8000/fridge/addIngredient", ing)
    .toPromise()
    .catch((reason) => {
      console.log("Error: sending Ingredient to fridge failed ",reason);
    })
    .then((response : any) => {
      console.log("fulfilled, Ingredient is added to fridge",response);
        this.f1.ingredients = response.ingredients.map((ing) => {
          return new Ingredient(ing.name,ing.quantity);
    })
  })
}
  
  
  addIngredient( ing : Ingredient){
    this.httpClient.post("http://localhost:8000/recipe/" + this.selectedRecipe.name + "/addIngredient", ing)
    .toPromise()
    .catch((reason) => {
     // console.log("Error: sending Ingredient failed ",reason);
    })
    .then((response : any) => {
      this.selectedRecipe.ingredients = response.ingredients.map((ing) => {
        return new Ingredient(ing.name,ing.quantity);
      });
    });
  }

  addInstruction(inst : string){
    console.log(inst);
    this.httpClient.post("http://localhost:8000/" + this.selectedRecipe.name + "/addInstruction",{in : inst})
    .toPromise()
    .then((response : any) => {
      console.log(response);
      this.selectedRecipe.instructions = response.instruction.instructions.map((value) => {
        return value;
      }) ;
    })
    .catch((reason) => {
      console.log("Error: adding new instruction failed", reason);
    })    
  }

  deleteIngredient(ing : Ingredient){
    this.httpClient.delete("http://localhost:8000/deleteIngredient/" + this.selectedRecipe.name + "/" + ing.name)
    .toPromise()
    .then((respone) => {
      console.log("deleting ingredient is fullfilled");
    })
    .catch((reason) => {
      console.log("deleting ingredient is failed");
    });
  }

  deleteFridgeIngredient(ing : Ingredient){
    this.httpClient.delete("http://localhost:8000/fridge/deleteIngredient/" + ing.name)
    .toPromise()
    .then((respone) => {
      console.log("deleting ingredient from fridge is fullfilled");
    })
    .catch((reason) => {
      console.log("deleting ingredient from fridge is failed");
    });
  }

  deleteRecipe(recipe:Recipe){
    this.httpClient.delete("http://localhost:8000/deleteRecipe/" + recipe.name)
    .toPromise()
    .then((response) => {
      console.log(response);
    })
    .catch((reason) => {
      console.log(reason);
    })
  }

  generateShoppigList(){
    this.ingredientsToShop = this.f1.checkRecipe(this.selectedRecipe)[1];
  }

  cloneRecipe(recipe:Recipe){
    let r = new Recipe(recipe.name,recipe.estimatedTime);
    for(let i of recipe.ingredients){
      r.addItem(i);
    }
    for(let i of recipe.instructions){
      r.instructions.push(i);
    }
    return r;
  }

  init(){
    this.getFridgeListFromServer();
    this.getRecipeListFromServer();
}}
