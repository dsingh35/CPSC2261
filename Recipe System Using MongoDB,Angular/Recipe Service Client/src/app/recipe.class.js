"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ingredient_class_1 = require("./ingredient.class");
class Recipe {
    constructor(name = "", time = 0) {
        this.ingredients = new Array();
        this.instructions = new Array();
        this.name = name;
        this.estimatedTime = time;
    }
    addItem(i) {
        for (let item of this.ingredients) {
            if (item.name == i.name) {
                item.add(i);
                return;
            }
        }
        this.ingredients.push(new ingredient_class_1.Ingredient(i.name, i.quantity));
    }
    addInstruction(inst) {
        this.instructions.push(inst);
    }
}
exports.Recipe = Recipe;
//# sourceMappingURL=recipe.class.js.map