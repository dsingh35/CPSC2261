import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule} from '@angular/Forms';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { ShoppingListComponent } from './shopping-list/shopping-list.component';
import { FridgeComponent } from './fridge/fridge.component';
import { RecipeModifyComponent } from './recipe-modify/recipe-modify.component';
import { RecipeService } from './recipe.service';

@NgModule({
  imports:[
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  declarations: [
    AppComponent,
    ShoppingListComponent,
    FridgeComponent,
    RecipeModifyComponent
  ],
  providers: [RecipeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
